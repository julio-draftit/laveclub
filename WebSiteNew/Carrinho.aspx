﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Carrinho.aspx.cs" Inherits="WebSiteNew.Carrinho" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="main-cesta" class="mt-5">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12 col-lg-8 mb-5">
                    <table class="minha-cesta table table-responsive-sm">
                        <tr class="table-desc">
                            <th class="first">Produto</th>
                            <th>Preço</th>
                            <th>Quantidade</th>
                            <th>Total</th>
                        </tr>
                        <tbody>
                            <asp:PlaceHolder ID="phCarrinho" runat="server"></asp:PlaceHolder>
                            
                           <%-- <tr class="cart-item">
                                <td class="product-item first">
                                    <div class="product-item-details">
                                        <a class="remove mr-3 text-danger">x</a><span class="cart-title"><a href="#">Bermuda</a></span>
                                    </div>
                                </td>
                                <td class="price" data-title="Price"><span class="money">R$ 10.00</span></td>
                                <td class="quantity" data-title="Quantity"><input type="text" value="3"></td>
                                <td class="total last" data-title="Total"><span class="money">R$ 30.00</span></td>
                            </tr>--%>
                        </tbody>
                    </table>    
                </div>
                <div class="col-12 col-md-12 col-lg-4">
                    <div class="mais-info mb-5">
                        <div class="table-desc">
                            <h6>Mais Informações</h6>
                        </div>
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td width="40%">Total dos Produtos:</td>
                                    <td>R$ <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label>
                                   </td>
                                </tr>
                                <tr>
                                    <td>Entrega/Coleta:</td>
                                    <td>R$ 5,00</td>
                                </tr>
                                <tr>
                                    <td>Total:</td>
                                    <td>R$ <asp:Label ID="lblSubTotal" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td colspan="2"><strong>Endereço de Entrega</strong></td>
                                </tr>
                                <tr>
                                    <td class="td-endereco" colspan="2"><span>Alameda Santos - Cerqueira César São Paulo - SP, Brasil</span></td>
                                </tr>
                                <tr>
                                    <td colspan="2"><button class="btn btn-lg">Fazer Pedido</button></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>    
      </section>
    <asp:HiddenField ID="hfUserID" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script>
        function deleteItem (id, valor){
            console.log(id);
            var DTO = {
                carrinhoID: id,
                userID: $("#ContentPlaceHolder1_hfUserID").val()
                };
            $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "Carrinho.aspx/delete",
                    data: JSON.stringify(DTO),
                    datatype: "json",
                    success: function (result) {
                        //do something
                        if (result.d == "salvo") {
                            console.log(result);
                            $("#ContentPlaceHolder1_var" + id).css("display", "none");
                            var total = parseFloat($("#ContentPlaceHolder1_lblTotal").text());
                            total = total - valor;
                            $("#ContentPlaceHolder1_lblTotal").text(total);

                            var total2 = parseFloat($("#ContentPlaceHolder1_lblSubTotal").text());
                            total2 = total2 - valor;
                            $("#ContentPlaceHolder1_lblSubTotal").text(total2);
                            console.log(total);
                        }
                        else {
                            alert("Você não tem mais produtos nos carrinhos.");
                            window.location.href = "avulso.aspx";

                        }
                    },
                    error: function (xmlhttprequest, textstatus, errorthrown) {
                        alert(" conection to the server failed ");
                        console.log("error: " + errorthrown);
                    }
                });//end of $.ajax()
        }
    </script>
</asp:Content>
