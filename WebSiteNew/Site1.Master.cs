﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LavBfl;
using LavEntity;

namespace WebSiteNew
{
    public partial class Site1 : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            getInfo();
        }
        public void getInfo()
        {
            if (Request.Cookies["UserSettings"] != null)
            {
                if(Request.Cookies["UserSettings"]["UserName"] != null)
                {
                    login.Visible = false;
                    logged.Visible = true;
                    login2.Visible = false;
                    logged2.Visible = true;
                    lblName.Text = Request.Cookies["UserSettings"]["UserName"];
                    lblName2.Text = Request.Cookies["UserSettings"]["UserName"];

                    
                }
                var carrinhoEntity = new CarrinhoEntity();
                var carrinhoBfl = new CarrinhoBfl();
                carrinhoEntity.UserID = Convert.ToInt32(Request.Cookies["UserSettings"]["UserID"]);
                carrinhoBfl.countCarrinho(carrinhoEntity);

                lblBadge.Text = carrinhoEntity.Counts;
                lblBadge2.Text = carrinhoEntity.Counts;
                if (carrinhoEntity.Counts == "0")
                {
                    badge.Visible = false;
                    badge2.Visible = false;
                    
                }
                else
                {
                    logged.Visible = true;
                    logged2.Visible = true;
                }
            }
            else
            {
                Random r = new Random();
                int num = r.Next();
                HttpCookie myCookie = new HttpCookie("UserSettings");
                myCookie["UserID"] = num.ToString();
                myCookie.Expires = DateTime.Now.AddDays(1d);
                Response.Cookies.Add(myCookie);
            }
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            var userEntity = new UserEntity();
            var userBfl = new UserBfl();

            userEntity.UserMail = txtMail.Text;
            userEntity.UserPassword = txtPass.Text;

            userBfl.PesquisarUsuario(userEntity);

            if(userEntity.UserName == "")
            {
                Response.Redirect("Login.aspx?error=failed");
            }
            else
            {
                HttpCookie myCookie = new HttpCookie("UserSettings");
                myCookie["UserName"] = userEntity.UserName;
                myCookie["UserMail"] = txtMail.Text;
                myCookie["UserID"] = Convert.ToString(userEntity.UserID);
                myCookie["UserPerfil"] = userEntity.UserPerfil;
                myCookie.Expires = DateTime.Now.AddDays(180d);
                Response.Cookies.Add(myCookie);

                login.Visible = false;
                logged.Visible = true;
                login2.Visible = false;
                logged2.Visible = true;

                lblName.Text = userEntity.UserName;
                lblName2.Text = userEntity.UserName;
            }
        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
            txtMail2.Text = "";
            txtName.Text = "";
            //txtPass.Text = "";
            txtTelefone.Text = "";
        }
    }
}