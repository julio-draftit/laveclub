﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="avulso.aspx.cs" Inherits="WebSiteNew.avulso" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="main-avulsos">
        <div class="container">
            <div class="round-logo">
                <img src="img/logo.svg">
            </div>
            <div class="main-avulsos_header">
                <h1>Avulsos</h1>
                <img src="img/t-shirt-ico.svg">
                <h2>Escolha suas peças</h2>
            </div>
        </div> 
    </section>
    <section id="itens-comuns">
        <div class="divisor">

            <h6>Itens Comuns</h6>
        </div>
        <div class="container">

            <div class="row">
                <asp:PlaceHolder ID="phItens" runat="server"></asp:PlaceHolder>
            </div>
        </div>
    </section>

    <%--<section id="itens-especiais">
        <div class="divisor">
            <h6>ITENS ESPECIAIS</h6>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="produto mb-4">
                        <div class="row justify-content-center mb-3">
                            <div class="col-md-12">
                                <div class="p-info">
                                    <p class="p-nome"><span>Tênis</span></p>
                                    <p class="p-obs">*sob consulta</p>
                                    <p class="p-consulta">A partir de R$ 50,00</p>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center mb-4">
                            <div class="col-6 col-md-5">
                                <div class="p-image">
                                    <img src="img/produtos/tenis.jpg">
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-6 col-md-7">
                                <div class="p-button">
                                    <div class="btn-quantidade mb-4">
                                        <button class="diminui-qtd">-</button><input type="text" value="0"><button class="aumenta-qtde">+</button>
                                    </div>
                                    <button class="btn py-2 my-2 my-sm-0">Adicionar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="produto mb-4">
                        <div class="row justify-content-center mb-3">
                            <div class="col-md-12">
                                <div class="p-info">
                                    <p class="p-nome"><span>Jaquetas de Couro</span></p>
                                    <p class="p-obs">*sob consulta</p>
                                    <p class="p-consulta">A partir de R$ 120,00</p>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center mb-4">
                            <div class="col-6 col-md-5">
                                <div class="p-image">
                                    <img src="img/produtos/jaqueta_couro.jpg">
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-6 col-md-7">
                                <div class="p-button">
                                    <div class="btn-quantidade mb-4">
                                        <button class="diminui-qtd">-</button><input type="text" value="0"><button class="aumenta-qtde">+</button>
                                    </div>
                                    <button class="btn py-2 my-2 my-sm-0">Adicionar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="produto mb-4">
                        <div class="row justify-content-center mb-3">
                            <div class="col-md-12">
                                <div class="p-info">
                                    <p class="p-nome"><span>Calças de Ski</span></p>
                                    <p class="p-obs">*sob consulta</p>
                                    <p class="p-consulta">A partir de R$ 40,00</p>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center mb-4">
                            <div class="col-6 col-md-5">
                                <div class="p-image">
                                    <img src="img/produtos/calca_ski.jpg">
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-6 col-md-7">
                                <div class="p-button">
                                    <div class="btn-quantidade mb-4">
                                        <button class="diminui-qtd">-</button><input type="text" value="0"><button class="aumenta-qtde">+</button>
                                    </div>
                                    <button class="btn py-2 my-2 my-sm-0">Adicionar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">                
                <div class="col-md-4">
                    <div class="produto mb-4">
                        <div class="row justify-content-center mb-3">
                            <div class="col-md-12">
                                <div class="p-info">
                                    <p class="p-nome"><span>Casacos de Couro</span></p>
                                    <p class="p-obs">*sob consulta</p>
                                    <p class="p-consulta">A partir de R$ 180,00</p>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center mb-4">
                            <div class="col-6 col-md-5">
                                <div class="p-image">
                                    <img src="img/produtos/casacodecouro.jpg">
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-6 col-md-7">
                                <div class="p-button">
                                    <div class="btn-quantidade mb-4">
                                        <button class="diminui-qtd">-</button><input type="text" value="0"><button class="aumenta-qtde">+</button>
                                    </div>
                                    <button class="btn py-2 my-2 my-sm-0">Adicionar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="produto mb-4">
                        <div class="row justify-content-center mb-3">
                            <div class="col-md-12">
                                <div class="p-info">
                                    <p class="p-nome"><span>Casacos de Ski</span></p>
                                    <p class="p-obs">*sob consulta</p>
                                    <p class="p-consulta">A partir de R$ 40,00</p>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center mb-4">
                            <div class="col-6 col-md-5">
                                <div class="p-image">
                                    <img src="img/produtos/casaco_ski.jpg">
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-6 col-md-7">
                                <div class="p-button">
                                    <div class="btn-quantidade mb-4">
                                        <button class="diminui-qtd">-</button><input type="text" value="0"><button class="aumenta-qtde">+</button>
                                    </div>
                                    <button class="btn py-2 my-2 my-sm-0">Adicionar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="produto mb-4">
                        <div class="row justify-content-center mb-3">
                            <div class="col-md-12">
                                <div class="p-info">
                                    <p class="p-nome"><span>Tapetes</span></p>
                                    <p class="p-obs">*sob consulta</p>
                                    <p class="p-consulta">A partir de R$ 40,00 por m²</p>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center mb-4">
                            <div class="col-6 col-md-5">
                                <div class="p-image">
                                    <img src="img/produtos/tapete.jpg">
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-6 col-md-7">
                                <div class="p-button">
                                    <div class="btn-quantidade mb-4">
                                        <button class="diminui-qtd">-</button><input type="text" value="0"><button class="aumenta-qtde">+</button>
                                    </div>
                                    <button class="btn py-2 my-2 my-sm-0">Adicionar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="produto mb-4">
                        <div class="row justify-content-center mb-3">
                            <div class="col-md-12">
                                <div class="p-info">
                                    <p class="p-nome"><span>Cortinas</span></p>
                                    <p class="p-obs">*sob consulta</p>
                                    <p class="p-consulta">A partir de R$ 30,00 altura</p>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center mb-4">
                            <div class="col-6 col-md-5">
                                <div class="p-image">
                                    <img src="img/produtos/cortina.jpeg">
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-6 col-md-7">
                                <div class="p-button">
                                    <div class="btn-quantidade mb-4">
                                        <button class="diminui-qtd">-</button><input type="text" value="0"><button class="aumenta-qtde">+</button>
                                    </div>
                                    <button class="btn py-2 my-2 my-sm-0">Adicionar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="produto mb-4">
                        <div class="row justify-content-center mb-3">
                            <div class="col-md-12">
                                <div class="p-info">
                                    <p class="p-nome"><span>Capas de Sofás</span></p>
                                    <p class="p-obs">*sob consulta</p>
                                    <p class="p-consulta">A partir de R$ 50,00 por assento</p>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center mb-4">
                            <div class="col-6 col-md-5">
                                <div class="p-image">
                                    <img src="img/produtos/capa_sofa.jpeg">
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-6 col-md-7">
                                <div class="p-button">
                                    <div class="btn-quantidade mb-4">
                                        <button class="diminui-qtd">-</button><input type="text" value="0"><button class="aumenta-qtde">+</button>
                                    </div>
                                    <button class="btn py-2 my-2 my-sm-0">Adicionar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="produto mb-4">
                        <div class="row justify-content-center mb-3">
                            <div class="col-md-12">
                                <div class="p-info">
                                    <p class="p-nome"><span>Toalhas de Mesa</span></p>
                                    <p class="p-obs">*sob consulta</p>
                                    <p class="p-consulta">A partir de R$ 40,00</p>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center mb-4">
                            <div class="col-6 col-md-5">
                                <div class="p-image">
                                    <img src="img/produtos/toalha_mesa.jpg">
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-6 col-md-7">
                                <div class="p-button">
                                    <div class="btn-quantidade mb-4">
                                        <button class="diminui-qtd">-</button><input type="text" value="0"><button class="aumenta-qtde">+</button>
                                    </div>
                                    <button class="btn py-2 my-2 my-sm-0">Adicionar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="produto mb-4">
                        <div class="row justify-content-center mb-3">
                            <div class="col-md-12">
                                <div class="p-info">
                                    <p class="p-nome"><span>Bichos de Pelúcia</span></p>
                                    <p class="p-obs">*sob consulta</p>
                                    <p class="p-consulta">A partir de R$ 20,00</p>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center mb-4">
                            <div class="col-6 col-md-5">
                                <div class="p-image">
                                    <img src="img/produtos/pelucia.jpg">
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-6 col-md-7">
                                <div class="p-button">
                                    <div class="btn-quantidade mb-4">
                                        <button class="diminui-qtd">-</button><input type="text" value="0"><button class="aumenta-qtde">+</button>
                                    </div>
                                    <button class="btn py-2 my-2 my-sm-0">Adicionar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>--%><asp:HiddenField ID="hfUserID" runat="server" />
    <div id="save">
        Produto adicionado com sucesso!
    </div>
    <style>
        #save {
            position: fixed;
            top: 0px;
            left: 0px;
            background: #50c750;
            color: white;
            padding: 20px;
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script>
        function addItem(id) {
            console.log(id, $("#prod" + id).val());
            if ($("#prod" + id).val() != "0") {
                var DTO = {
                    quantidade: $("#prod" + id).val(),
                    itemID: id,
                    userID: $("#ContentPlaceHolder1_hfUserID").val()
                };
                console.log(DTO);
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "avulso.aspx/insert",
                    data: JSON.stringify(DTO),
                    datatype: "json",
                    success: function (result) {
                        //do something
                        if (result.d == "salvo") {
                            console.log(result);
                            $("#prod" + id).val("0");
                            $('#save').fadeIn('slow');
                            $("#save").css("display", "block");
                            setTimeout(function(){ $('#save').fadeOut('slow');$("#save").css("display", "none"); }, 3000);
                        }
                        else {
                            alert("Falha na comunicação. Tente novamente.");
                        }
                    },
                    error: function (xmlhttprequest, textstatus, errorthrown) {
                        alert(" conection to the server failed ");
                        console.log("error: " + errorthrown);
                    }
                });//end of $.ajax()
            }
        }
        function remove(id) {
            if ($("#prod" + id).val() != "0") {
                var qtd = $("#prod" + id).val();
                qtd = parseInt(qtd) - 1;
                $("#prod" + id).val(qtd);
            }
        }
        function add(id) {
            var qtd = $("#prod" + id).val();
            qtd = parseInt(qtd) + 1;
            $("#prod" + id).val(qtd);
        }
    </script>
</asp:Content>
