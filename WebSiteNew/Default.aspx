﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebSiteNew.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <nav class="subnav">
        <div class="container">
            <div class="packs">
                <a href="#executivo">
                    <div class="pack-item" id="pack-exec">
                        <img src="img/shirt-ico.svg">
                        <div class="detalhes"><p>Pack<span class="pack-name">Executivo</span></p></div>
                    </div>
                </a>
                <a href="#diadia">
                    <div class="pack-item" id="pack-diadia">
                        <img src="img/bag-ico.svg">
                        <div class="detalhes"><p>Pack<span class="pack-name">Dia a Dia</span></p></div>
                    </div>
                </a>
                <a href="#avulso">
                <div class="pack-item" id="pack-avulso">
                    <img src="img/t-shirt-ico.svg">
                    <div class="detalhes"><p>Pack<span class="pack-name">Avulso</span></p></div>
                </div>
                </a>
        </div>
    </nav>
     <section id="main">
        <div class="container">
            <div class="logo">
                <img src="img/logo.svg">
            </div>
            <div class="main-txt">
                <h1>Roupa suja não se lava mais em casa</h1>
            </div>
        </div>
    </section>
    <section id="about">
        <div class="container d-flex justify-content-center mt-5"></div>
            <div class="sliderrr">
                <div>
                    <img src="img/pic1.jpg" />
                </div>
                <div>
                    <img src="img/pic1.jpg" />  
                </div>
                <div>
                    <img src="img/pic1.jpg" />  
                </div>
            </div>
        </div>

        <div class="container">
            <div class="main-txt">
                <h4 class="mt-5">Como funciona o LaveClub</h4>
            </div>
            <div class="row d-flex justify-content-center">
                <div class="steps mt-5">
                    <img src="img/steps.svg">
                </div>
            </div>
            <div class="main-txt">
                <h4 class="mt-5">Em 3 dias úteis suas roupas de volta até você.</h4>
            </div>
        </div>
    </section>

    <section id="diadia">
        <div class="sec-banner">
            <div class="container c-relative">
                <div class="badge-price">
                    <h2>PACK</h2>
                    <h1>R$</h1>
                    <h2>139,00</h2>
                </div>
                <img src="img/bag_white-ico.svg" class="banner-ico">
                <h1>Dia a Dia</h1>
                <h4>Coloque até 30 peças de roupa na BAG!</h4>
                <h5 class="mt-5">Roupas do dia a dia:</h5>
                <h6>Lençóis, fronhas, toalhas de banho, rosto e piso, pijamas, jeans, fitness, bermudas...</h6>
            </div>
        </div>
        <div class="container">
            <div class="row mt-5 justify-content-center">
                <div class="col-md-4">
                    <div class="plano-opt">
                        <h3>PLANO</h3>
                        <h1>2x/Mês</h1>
                        <h2 class="mt-4 text-white">R$ 198,00</h2>
                        <h4 class="mt-3"><strike>R$ 230,00</strike></h4>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="plano-opt">
                        <h3>PLANO</h3>
                        <h1>4x/Mês</h1>
                        <h2 class="mt-4 text-white">R$ 369,00</h2>
                        <h4 class="mt-3"><strike>R$ 436,00</strike></h4>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="plano-opt">
                        <h3>PLANO</h3>
                        <h1>8x/Mês</h1>
                        <h2 class="mt-4 text-white">R$ 699,00</h2>
                        <h4 class="mt-3"><strike>R$ 872,00</strike></h4>
                    </div>
                </div>
            </div>
            <h5 class="text-center mt-5">* Pacote adicional para assinantes > R$ 99,00</h5>
        </div>
    </section>

    <section id="executivo">
            <div class="sec-banner">
                <div class="container c-relative">
                    <div class="badge-price">
                        <h2>PACK</h2>
                        <h1>R$</h1>
                        <h2>139,00</h2>
                    </div>
                    <img src="img/shirt_white-ico.svg" class="banner-ico">
                    <h1>Executivo</h1>
                    <h4>12 Peças</h4>
                    <h5 class="mt-5">Roupas executivas:</h5>
                    <h6>Camisas, polos, calças, blusas, saia, blazer, vestido simples.</h6>
                </div>
            </div>
            <div class="container">
                <div class="row mt-5 justify-content-center">
                    <div class="col-md-6">
                        <div class="plano-opt">
                            <h3>PLANO</h3>
                            <h1>2x/Mês</h1>
                            <h2 class="mt-4 text-white">R$ 198,00</h2>
                            <h4 class="mt-3"><strike>R$ 218,00</strike></h4>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="plano-opt">
                            <h3>PLANO</h3>
                            <h1>4x/Mês</h1>
                            <h2 class="mt-4 text-white">R$ 369,00</h2>
                            <h4 class="mt-3"><strike>R$ 436,00</strike></h4>
                        </div>
                    </div>
                </div>
                <h5 class="text-center mt-5">* Pacote adicional para assinantes > R$ 99,00</h5>
            </div>
        </section>

    <section id="avulso" class="mb-5">
        <div class="sec-banner">
            <div class="container">
                <img src="img/t-shirt_big-ico.svg" class="banner-ico">
                <h1>Avulso</h1>
                <h4>Faça também seu pedido avulso</h4>
                <h5>edredons, vestido, ternos, roupas delicadas, tênis, tapetes</h5>
                <a href="avulsos.html" class="btn btn-secondary py-2 px-5 mt-4">Escolha sua peça</a>
            </div>
        </div>
    </section>

    <%--<section id="cobertura">
        <div class="container">
            <div class="main-txt">
                <h2 class="mt-5">Área de Cobertura</h2>
            </div>
        </div>
        <!-- <iframe src="https://www.google.com/maps/d/embed?mid=13ifGmFc23IrhRZDFptsAnZi8P1wat-F_" width="100%" height="480"></iframe> -->
        <div class="main-map map-container" id="js-map">
        </div>
            <div class="logo-mini mt-5">
                <img src="img/logo.svg">
            </div>
        </div>
    </section>--%>

   
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
</asp:Content>
