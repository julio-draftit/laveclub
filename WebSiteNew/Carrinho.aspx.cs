﻿using LavBfl;
using LavEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace WebSiteNew
{
    public partial class Carrinho : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Cookies["UserSettings"] != null)
            {
                hfUserID.Value = Request.Cookies["UserSettings"]["UserID"];
                var carrinhoEntity1 = new CarrinhoEntity();
                var carrinhoBfl1 = new CarrinhoBfl();
                carrinhoEntity1.UserID = Convert.ToInt32(Request.Cookies["UserSettings"]["UserID"]);
                carrinhoBfl1.countCarrinho(carrinhoEntity1);

                if (carrinhoEntity1.Counts == "0")
                {
                    Response.Redirect("avulso.aspx");
                }
            }

            getCarrinho();
        }
        public void getCarrinho()
        {
            var carrinhoEntity = new CarrinhoEntity();
            var carrinhoBfl = new CarrinhoBfl();
            if (Request.Cookies["UserSettings"] != null)
            {
                carrinhoEntity.UserID = Convert.ToInt32(Request.Cookies["UserSettings"]["UserID"]);
            }
            List<CarrinhoEntity> listCarrinho = carrinhoBfl.listCarrinho(carrinhoEntity);
            decimal total = 0;
            decimal subtotal = 0;
            for (int i = 0; i < listCarrinho.Count; i++)
            {
                var car = listCarrinho[i];

                decimal tot = Convert.ToDecimal(car.ItemPrice) * car.Quantidade;
                HtmlGenericControl tr = new HtmlGenericControl("tr");
                tr.Attributes["class"] = "cart-item";
                tr.ID = "var" + car.CarrinhoID;
                tr.InnerHtml = "<td class=\"product-item first\">" +
                                    "<div class=\"product -item-details\">" +
                                        "<a href=\"javascript:deleteItem('" + car.CarrinhoID + "', "+ tot + ")\" class=\"remove mr-3 text-danger\">x</a><span class=\"cart-title\"><a href=\"#\">" + car.ItemName + "</a></span> " +
                                    "</div> " +
                                "</td>" +
                                "<td class=\"price\" data-title=\"Price\"><span class=\"money\">R$ " + car.ItemPrice + "</span></td>" +
                                "<td class=\"quantity\" data-title=\"Quantity\"><span class=\"money\">" + car.Quantidade + "</span></td>" +
                                "<td class=\"total last\" data-title=\"Total\"><span class=\"money\">R$ " + tot + "</span></td>";

                phCarrinho.Controls.Add(tr);
                total = total + tot;
                
                lblTotal.Text = total.ToString();

            }
            subtotal = total + 5;
            lblSubTotal.Text = subtotal.ToString();
        }

        [WebMethod]
        public static string delete(int carrinhoID, int userID)
        {
            var carrinhoEntity = new CarrinhoEntity();
            var carrinhoBfl = new CarrinhoBfl();

            carrinhoEntity.CarrinhoID = carrinhoID;

            if (carrinhoBfl.deleteCarrinho(carrinhoEntity))
            {
                var carrinhoEntity1 = new CarrinhoEntity();
                var carrinhoBfl1 = new CarrinhoBfl();
                carrinhoEntity1.UserID = userID;
                carrinhoBfl1.countCarrinho(carrinhoEntity1);

                
                if (carrinhoEntity1.Counts == "0")
                {
                    return "vazio";
                }
                else
                {
                    return "salvo";
                }
                
            }
            else
            {
                return "0";
            }
        }
    }
}