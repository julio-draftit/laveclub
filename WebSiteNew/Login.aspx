﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="WebSiteNew.Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .divLogin input {
            border: 1px solid #bcbcbc;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="itens-comuns">
        <div id="fail" visible="false" runat="server" class="divisor" style="background-color: #b72727;">
            <h6>E-mail ou senha inválido.</h6>
        </div>
        <br />
        <br />
        <div class="container">

            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <div class="divLogin">
                        <h2>Login</h2>
                        <p>Faça o login agora:</p>
                        <div class="form-group">
                            <asp:TextBox ID="txtMail"  type="email" class="form-control" runat="server" placeholder="E-mail"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="txtPass" class="form-control" runat="server" type="password" placeholder="Senha"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <asp:Button ID="btnLogin" runat="server" class="btn btn-primary btn-block" Text="Entrar" OnClick="btnLogin_Click" />
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="divLogin">
                        <h2>Registrar-se</h2>
                        <p>Não tem cadastro? Faça um agora, é rápido e prático.</p>
                        <div class="form-group">
                            <asp:TextBox ID="txtName" class="form-control" runat="server" placeholder="Nome Completo"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="txtMail2" type="email" class="form-control" runat="server" placeholder="E-mail"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="txtCEP" class="form-control" runat="server" placeholder="CEP"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="txtEndereco" class="form-control" runat="server" placeholder="Endereço"></asp:TextBox>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <asp:TextBox ID="txtNumero" class="form-control my-1 mr-sm-2" runat="server" placeholder="Número"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <asp:TextBox ID="txtComplemento" class="form-control my-1 mr-sm-2" runat="server" placeholder="Complemento"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <asp:TextBox ID="txtBairro" class="form-control my-1 mr-sm-2" runat="server" placeholder="Bairro"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <asp:TextBox ID="txtCidade" class="form-control" runat="server" placeholder="Cidade"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <asp:TextBox ID="txtEstado" class="form-control" runat="server" placeholder="Estado"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="txtTelefone" class="form-control" runat="server" placeholder="Telefone"></asp:TextBox>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <asp:TextBox ID="txtSenha" class="form-control " runat="server" type="password" placeholder="Senha"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group  mb-2">
                                    <asp:TextBox ID="txtCoSenha" class="form-control" runat="server" type="password" placeholder="Corfirme sua senha"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Button ID="btnRegister" runat="server" class="btn btn-primary btn-block" Text="Cadastrar" OnClick="btnRegister_Click" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="js/jquery.mask.js"></script>
    <script>
        $(document).ready(function () {
            $('#ContentPlaceHolder1_txtCEP').mask('00000-000');
             $('#ContentPlaceHolder1_txtTelefone').mask('(00) 00000-0000');
              //$('.cpf').mask('000.000.000-00', {reverse: true});

            function limpa_formulário_cep() {
                // Limpa valores do formulário de cep.
                $("#ContentPlaceHolder1_txtEndereco").val("");
                $("#ContentPlaceHolder1_txtBairro").val("");
                $("#ContentPlaceHolder1_txtCidade").val("");
                $("#ContentPlaceHolder1_txtEstado").val("");
                //$("#ibge").val("");
            }
            
            //Quando o campo cep perde o foco.
            $("#ContentPlaceHolder1_txtCEP").blur(function() {

                //Nova variável "cep" somente com dígitos.
                var cep = $(this).val().replace(/\D/g, '');

                //Verifica se campo cep possui valor informado.
                if (cep != "") {

                    //Expressão regular para validar o CEP.
                    var validacep = /^[0-9]{8}$/;

                    //Valida o formato do CEP.
                    if(validacep.test(cep)) {

                        //Preenche os campos com "..." enquanto consulta webservice.
                        $("#ContentPlaceHolder1_txtEndereco").val("...");
                        $("#ContentPlaceHolder1_txtBairro").val("...");
                        $("#ContentPlaceHolder1_txtCidade").val("...");
                        $("#ContentPlaceHolder1_txtEstado").val("...");
                        //$("#ibge").val("...");

                        //Consulta o webservice viacep.com.br/
                        $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                            if (!("erro" in dados)) {
                                //Atualiza os campos com os valores da consulta.
                                $("#ContentPlaceHolder1_txtEndereco").val(dados.logradouro);
                                $("#ContentPlaceHolder1_txtBairro").val(dados.bairro);
                                $("#ContentPlaceHolder1_txtCidade").val(dados.localidade);
                                $("#ContentPlaceHolder1_txtEstado").val(dados.uf);
                                //$("#ibge").val(dados.ibge);
                            } //end if.
                            else {
                                //CEP pesquisado não foi encontrado.
                                limpa_formulário_cep();
                                alert("CEP não encontrado.");
                            }
                        });
                    } //end if.
                    else {
                        //cep é inválido.
                        limpa_formulário_cep();
                        alert("Formato de CEP inválido.");
                    }
                } //end if.
                else {
                    //cep sem valor, limpa formulário.
                    limpa_formulário_cep();
                }
            });
        });
    </script>
</asp:Content>
