// -------------- Scroll Suave Navegação
$(document).ready(function(){
  $('.nav-item a[href^="#"], .packs a[href^="#"]').on('click', function(e) {
    e.preventDefault();
    var id = $(this).attr('href'),
    targetOffset = $(id).offset().top;
      
    $('html, body').animate({ 
      scrollTop: targetOffset - 100
    }, 900);
  });

  // -------------- lick Slider 
  $('.sliderrr').slick({
    arrows: false,
    dots: true,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 6000,
    speed: 800,
    slidesToShow: 1,
    adaptiveHeight: true
  });
  // Botão Custom do Slick sem texto
  $('.slick-dots li button').html('');

  //-------------- Botão Menu Mobile
  $('#btn-toggler').click(function(){
    $('.menu-mobile').css('transform', 'translateX(0px)');
    $('.menu-mobile').addClass('active');
    if($('.menu-mobile').hasClass('active')){
      $('body').css('overflow', 'hidden');
    }
    $('#button-menu_close').click(function(){
      $('.menu-mobile').css('transform', 'translateX(-300px)');
      $('body').css('overflow', 'auto');
    })
  })

  //-------------- Scroll Suave Mobile
  $('.mobile-nav a[href^="#"]').on('click', function(e) {
    e.preventDefault();
    // Recolhe o Menu e deixa overflow livre
    $('.menu-mobile').css('transform', 'translateX(-300px)');
    $('body').css('overflow', 'auto');
    // Vai até o elemento
    var id = $(this).attr('href'),
    targetOffset = $(id).offset().top;
    // Scroll Suave com delay para fechar o menu
    $('html, body').stop().delay(500).animate({ 
      scrollTop: targetOffset - 100
    }, 900);
  });

  // -------------- Jquery Mask
  $('#locCep').mask('00000-000');
});