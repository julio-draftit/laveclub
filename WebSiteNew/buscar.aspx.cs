﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebSiteNew
{
    public partial class buscar : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnRedirect_Click(object sender, EventArgs e)
        {
            HttpCookie myCookie = new HttpCookie("Location");
            myCookie["Lavanderia"] = hfLavanderia.Value;
            myCookie.Expires = DateTime.Now.AddDays(180d);
            Response.Cookies.Add(myCookie);
            Response.Redirect("Default.aspx");
        }
    }
}