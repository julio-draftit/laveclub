﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="buscar.aspx.cs" Inherits="WebSiteNew.buscar" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>LaveClub</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link href="https://fonts.googleapis.com/css?family=Montserrat|Open+Sans&display=swap" rel="stylesheet">
    <style>
        body {
            background-image: url('img/laundry.jpg');
            background-repeat: no-repeat;
            background-size: 100% 100vh;
            font-family: 'Montserrat', sans-serif;
        }

        .location {
            background-color: rgba(0,0,0,0.7);
            padding: 20px;
            color: #fff;
            margin-top: 20%;
        }


        .btn-primary {
            color: #fff;
            background-color: #62acb6;
            border-color: #62acb6;
        }

            .btn-primary:hover {
                color: #62acb6;
                background-color: #fff;
                border-color: #fff;
            }

        hr {
            border-top: 1px solid rgba(255, 255, 255, .3);
        }

        .navbar {
            background-color: rgba(0,0,0,0.7);
        }

        .navbar-light .navbar-nav .nav-link {
            color: #fff !important;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <nav class="navbar navbar-expand-lg navbar-light w-100">
            <!-- Brand and toggle get grouped for better mobile display -->
            <a class="navbar-brand logo_h" href="Default.aspx">
                <img src="img/logo-white.png" alt="" />
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse offset w-100" id="navbarSupportedContent">
                <div class="row w-100 mr-0">
                    <div class="col-lg-7 pr-0">
                        <ul class="nav navbar-nav center_nav pull-right">
                            <li class="nav-item active">
                                <a class="nav-link" href="Login.aspx">Entrar</a>
                            </li>
                        </ul>
                    </div>

                    <div class="col-lg-5 pr-0">
                        <ul class="nav navbar-nav navbar-right right_nav pull-right">

                            <li class="nav-item">
                                <a href="#" class="icons">
                                    <i class="ti-shopping-cart"></i>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="#" class="icons">
                                    <i class="ti-user" aria-hidden="true"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-lg-4 offset-lg-4 col-md-6 offset-md-3 col-sm-6 offset-sm-3 col-xs-12 align-self-center text-center">
                    <div class="location">
                        <h4>Informe sua localização</h4>
                        <a class="btn btn-primary btn-lg btn-block" href="javascript:getLocation();" role="button">USAR MINHA LOCALIZAÇÃO</a>
                        <hr />
                        <p>ou informe o CEP</p>

                        <div class="input-group mb-3">
                            <input id="txtCEP" type="text" class="form-control" placeholder="CEP" aria-label="CEP" aria-describedby="button-addon2">
                            <div class="input-group-append">
                                <a href="javascript:sendCEP();" class="btn btn-primary" type="button" id="button-addon2">BUSCAR</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
        <asp:HiddenField ID="hfLavanderia" runat="server" />
        <asp:Button ID="btnRedirect" runat="server" Text="" OnClick="btnRedirect_Click" style="background: transparent;border: none;" />

        <script src="js/jquery.min.js"></script>
        <%--<script src="js/popper.js"></script>--%>
        <script src="js/bootstrap.min.js"></script>
        <%--<script src="js/stellar.js"></script>
        <script src="vendors/lightbox/simpleLightbox.min.js"></script>
        <script src="vendors/nice-select/js/jquery.nice-select.min.js"></script>
        <script src="vendors/isotope/imagesloaded.pkgd.min.js"></script>
        <script src="vendors/isotope/isotope-min.js"></script>
        <script src="vendors/owl-carousel/owl.carousel.min.js"></script>
        <script src="js/jquery.ajaxchimp.min.js"></script>
        <script src="vendors/counter-up/jquery.waypoints.min.js"></script>
        <script src="vendors/counter-up/jquery.counterup.js"></script>
        <script src="js/mail-script.js"></script>
        <script src="js/theme.js"></script>--%>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.4/lodash.min.js"></script>
        <script>
            function sendCEP() {
                var set = $("#txtCEP").val();
                setPostion(set);
            }
            function getLocation() {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(showPosition);
                } else {
                    console.log("Geolocation is not supported by this browser.");
                }
            }

            function showPosition(position) {
                setPostion2(position.coords.latitude, position.coords.longitude);
            }
            var regioes = [
                { "CEP": "05014-000", "Nome": "Inovelave" },
                { "CEP": "01531-000", "Nome": "Inovelave" },
                { "CEP": "05685-010", "Nome": "Evolution Real Parque" },
                { "CEP": "01453-011", "Nome": "Evolution Itaim" },
                { "CEP": "03165-001", "Nome": "Personal" }
            ]

            function setPostion2(lat, long) {
                var start = lat + "," + long;
                var min = 0;
                var cep = "";
                var name2 = "";
                var kms = [];
                for (var l = 0; l < regioes.length; l++) {
                    var rg = regioes[l];
                    
                    var end = rg.CEP;

                    kms = getKMs(start, end, rg.Nome, rg.CEP);
                    if (l == regioes.length - 1) {
                        //console.log("KMS", kms);
                    }
                }

                setTimeout(function () {
                    for (var k = 0; k < kms.length; k++) {
                        var aa = kms[k];

                        if (k == 0) {
                            min = aa.distancia;
                            name2 = aa.nm;
                            cep = aa.cp;
                        }
                        else {
                            if (min > aa.distancia) {
                                min = aa.distancia;
                                name2 = aa.nm;
                                cep = aa.cp;
                            }
                        }
                        
                    }
                    //LIMIT DE KMS
                    alert("A Lavanderia próximo a você é " + name2);
                    $("#hfLavanderia").val(name2);
                    console.log(min, name2, cep);
                    document.getElementById("btnRedirect").click();
                }, 1000);

            }
            var value = [];
            function getKMs(start, end, nome,cep) {

                var directionsService = new google.maps.DirectionsService();
                var request = {
                    origin: start,
                    destination: end,
                    travelMode: 'DRIVING'
                };
                directionsService.route(request, function (result, status) {
                    if (status == 'OK') {
                        var k = result.routes[0].legs[0].distance.text;
                        k = k.replace(" km", "");
                        var aux = { distancia: parseInt(k), nm: nome, cp: cep };
                        value.push(aux);
                        return result.routes[0].legs[0].distance.text;
                        ////console.log(result);
                        //var km = result.routes[0].legs[0].distance.text;
                        //km = km.replace(" km", "");
                        //var rout = parseFloat(km);
                        //console.log(rout);
                        //kms.push(rout);
                        //var min = Math.min(kms);
                        //console.log("Min",min);
                        //console.log(kms);

                    } else {
                        return result;

                    }
                });
                return value;
            }
            function getMin() {
                console.log("KM", kms);
                var min = Math.min(kms);
                console.log("Min", min);
            }
            //teste
            function setPostion(cep2) {
                var start = cep2;
                var min = 0;
                var cep = "";
                var name2 = "";
                var kms = [];
                for (var l = 0; l < regioes.length; l++) {
                    var rg = regioes[l];
                    
                    var end = rg.CEP;

                    kms = getKMs(start, end, rg.Nome, rg.CEP);
                    if (l == regioes.length - 1) {
                        //console.log("KMS", kms);
                    }
                }

                setTimeout(function () {
                    for (var k = 0; k < kms.length; k++) {
                        var aa = kms[k];

                        if (k == 0) {
                            min = aa.distancia;
                            name2 = aa.nm;
                            cep = aa.cp;
                        }
                        else {
                            if (min > aa.distancia) {
                                min = aa.distancia;
                                name2 = aa.nm;
                                cep = aa.cp;
                            }
                        }
                        
                    }
                    //LIMIT DE KMS
                    alert("A Lavanderia próximo a você é " + name2);
                    console.log(min, name2, cep);
                    $("#hfLavanderia").val(name2);
                    document.getElementById("btnRedirect").click();
                }, 1000);
            }
        </script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBF0UMuBET_USSX06Rm3nDUZtI3eTonT2Y">
        </script>
    </form>
</body>
</html>
