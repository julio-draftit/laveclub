﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using LavBfl;
using LavEntity;

namespace WebSiteNew
{
    public partial class avulso : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Cookies["UserSettings"] != null)
            {
                hfUserID.Value = Request.Cookies["UserSettings"]["UserID"];
            }
                getItens();
        }
        [WebMethod]
        public static string insert(int quantidade, int itemID, int userID)
        {
            var carrinhoEntity = new CarrinhoEntity();
            var carrinhoBfl = new CarrinhoBfl();

            carrinhoEntity.Quantidade = quantidade;
            carrinhoEntity.ItemID = itemID;
            carrinhoEntity.UserID = userID;

            if (carrinhoBfl.insertCarrinho(carrinhoEntity))
            {
                return "salvo";
            }
            else
            {
                return "0";
            }
        }
        public void getItens()
        {
            var itenEntity = new ItemEntity();
            var itemBfl = new ItemBfl();

            List<ItemEntity> listItem = itemBfl.listItens(itenEntity);

            for(int i = 0; i < listItem.Count; i++)
            {
                var item = listItem[i];

                HtmlGenericControl div = new HtmlGenericControl("div");
                div.Attributes["class"] = "col-md-4";
                div.InnerHtml = "<div class=\"produto mb-4\">"+
                        "<div class=\"row justify-content-center mb-3\">"+
                            "<div class=\"col-md-12\">"+
                                "<div class=\"p-info\">"+
                                    "<p class=\"p-nome\"><span>"+ item.ItemName +"</span></p>"+
                                    "<p class=\"p-preco\">R$ " + item.ItemPrice + "</p>" +
                                "</div>" +
                            "</div>" +
                        "</div>" +
                        "<div class=\"row justify-content-center mb-4\">"+
                            "<div class=\"col-6 col-md-5\">"+
                                "<div class=\"p-image\">"+
                                    "<img src=\"" + item.ItemImage + "\"> " +
                                "</div> " +
                            "</div> " +
                        "</div> " +
                        "<div class=\"row justify-content-center\">"+
                            "<div class=\"col-6 col-md-7\">"+
                                "<div class=\"p-button\">"+
                                    "<div class=\"btn-quantidade mb-4\">"+
                                        "<a href=\"javascript:remove(" + item.ItemID + ");\" class=\"diminui-qtd\">-</a><input id=\"prod"+ item.ItemID +"\" type=\"text\" value=\"0\"><a href=\"javascript:add(" + item.ItemID + ");\" class=\"aumenta-qtde\">+</a>" +
                                    "</div>" +
                                    "<a href=\"javascript:addItem("+ item.ItemID +");\" class=\"btn py-2 my-2 my-sm-0\">Adicionar</a>"+
                                "</div>" +
                            "</div>" +
                        "</div>" +
                    "</div>";

                phItens.Controls.Add(div);
            }
        }
    }
}