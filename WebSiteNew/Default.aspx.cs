﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebSiteNew
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Response.Redirect("buscar.aspx");
            if (Request.Cookies["Location"] == null)
            {
                Response.Redirect("buscar.aspx");
            }
        }
    }
}