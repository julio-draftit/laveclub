﻿using LavBfl;
using LavEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebSiteNew
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["error"] == "failed")
            {
                fail.Visible = true;
            }
            if (Request.Cookies["UserSettings"] != null)
            {
                if (Request.Cookies["UserSettings"]["UserName"] != null)
                {
                    Response.Redirect("Default.aspx");
                }
            }
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            var userEntity = new UserEntity();
            var userBfl = new UserBfl();

            userEntity.UserMail = txtMail.Text;
            userEntity.UserPassword = txtPass.Text;

            userBfl.PesquisarUsuario(userEntity);

            if (userEntity.UserName == "")
            {
                Response.Redirect("Login.aspx?error=failed");
            }
            else
            {
                HttpCookie myCookie = new HttpCookie("UserSettings");
                myCookie["UserName"] = userEntity.UserName;
                myCookie["UserMail"] = txtMail.Text;
                myCookie["UserID"] = Convert.ToString(userEntity.UserID);
                myCookie["UserPerfil"] = userEntity.UserPerfil;
                myCookie.Expires = DateTime.Now.AddDays(180d);
                Response.Cookies.Add(myCookie);

                Response.Redirect("Default.aspx");
            }
        }

        protected void btnRegister_Click(object sender, EventArgs e)
        {
            var userEntity = new UserEntity();
            var userBfl = new UserBfl();

            userEntity.UserName = txtName.Text;
            userEntity.UserMail = txtMail2.Text;
            userEntity.UserCEP = txtCEP.Text;
            userEntity.UserEndereco = txtEndereco.Text;
            userEntity.UserNumero = txtNumero.Text + " / " + txtComplemento.Text;
            userEntity.UserBairro = txtBairro.Text;
            userEntity.UserCidade = txtCidade.Text;
            userEntity.UserEstado = txtEstado.Text;
            userEntity.UserTelefone = txtTelefone.Text;
            userEntity.UserPassword = txtSenha.Text;

            if (userBfl.insertUsuario(userEntity))
            {
                var userEntity2 = new UserEntity();
                var userBfl2 = new UserBfl();

                userEntity2.UserMail = txtMail2.Text;
                userEntity2.UserPassword = txtSenha.Text;

                userBfl2.PesquisarUsuario(userEntity2);

                if (userEntity2.UserName == "")
                {
                    //Response.Redirect("Login.aspx?error=failed");
                }
                else
                {
                    HttpCookie myCookie = new HttpCookie("UserSettings");
                    myCookie["UserName"] = userEntity2.UserName;
                    myCookie["UserMail"] = txtMail.Text;
                    myCookie["UserID"] = Convert.ToString(userEntity2.UserID);
                    myCookie["UserPerfil"] = userEntity2.UserPerfil;
                    myCookie.Expires = DateTime.Now.AddDays(180d);
                    Response.Cookies.Add(myCookie);

                    Response.Redirect("Default.aspx");
                }
            }
        }
    }
}