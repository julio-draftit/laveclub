﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="sobre.aspx.cs" Inherits="WebSiteNew.sobre" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="main-about">
        <div class="container">
            <div class="round-logo">
                <img src="img/logo.svg">
            </div>
            <div class="main-about_msg">
                <h1>Quem Somos</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque convallis sem mi, vel volutpat risus ultricies nec. Vivamus tellus lacus, tempor a convallis at, finibus eu felis. Nam sed arcu arcu. Aliquam accumsan ultrices lacus at finibus. Donec convallis in dui nec finibus. Sed ac mollis orci. Nulla facilisi. Pellentesque maximus eget augue quis luctus. Ut in commodo nulla. Nam arcu arcu, sagittis non arcu non, tincidunt pretium purus. In ornare, nulla sed rutrum efficitur, augue lacus pharetra risus, eu porta mauris est quis tellus. Suspendisse ac efficitur lacus. Proin malesuada sit amet neque vel pellentesque.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque convallis sem mi, vel volutpat risus ultricies nec. Vivamus tellus lacus, tempor a convallis at, finibus eu felis. Nam sed arcu arcu. Aliquam accumsan ultrices lacus at finibus. Donec convallis in dui nec finibus. Sed ac mollis orci. Nulla facilisi. Pellentesque maximus eget augue quis luctus. Ut in commodo nulla. Nam arcu arcu, sagittis non arcu non, tincidunt pretium purus. In ornare, nulla sed rutrum efficitur, augue lacus pharetra risus, eu porta mauris est quis tellus. Suspendisse ac efficitur lacus. Proin malesuada sit amet neque vel pellentesque.</p>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
</asp:Content>
