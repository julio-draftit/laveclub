﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LavEntity
{
    public class ItemEntity
    {
        public int ItemID { get; set; }
        public string ItemName { get; set; }
        public string ItemPrice { get; set; }
        public string ItemType { get; set; }
        public string ItemImage { get; set; }
    }
}
