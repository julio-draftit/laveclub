﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LavEntity
{
    public class CarrinhoEntity
    {
        public int CarrinhoID { get; set; }
        public int UserID { get; set; }
        public int ItemID { get; set; }
        public int Quantidade { get; set; }
        public DateTime DateCreate { get; set; }
        public string CarrinhoStatus { get; set; }
        public string Counts { get; set; }

        public string ItemName { get; set; }
        public string ItemPrice { get; set; }
        public string ItemType { get; set; }
        public string ItemImage { get; set; }
    }
}
