﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LavEntity
{
    public class UserEntity
    {
        public int UserID { get; set; }
        public string UserName { get; set; }
        public string UserMail { get; set; }
        public string UserPassword { get; set; }
        public string UserStatus { get; set; }
        public DateTime UserDateCreate { get; set; }
        public string UserPerfil { get; set; }
        public string UserCEP  { get; set; }
        public string UserEndereco { get; set; }
        public string UserNumero  { get; set; }
        public string UserBairro  { get; set; }
        public string UserCidade  { get; set; }
        public string UserEstado  { get; set; }
        public string UserTelefone  { get; set; }
    }
}
