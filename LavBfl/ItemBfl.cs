﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LavData;
using LavEntity;

namespace LavBfl
{
    public class ItemBfl
    {
        public List<ItemEntity> listItens(ItemEntity itemEntity)
        {
            try
            {
                var itemData = new ItemData();

                return itemData.listItens(itemEntity);

            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
