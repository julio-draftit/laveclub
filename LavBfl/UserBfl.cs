﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LavData;
using LavEntity;

namespace LavBfl
{
    public class UserBfl
    {
        public UserEntity PesquisarUsuario(UserEntity userEntity)
        {
            try
            {
                // Criando uma instancia da classe User contida na camada data
                var userData = new UserData();

                // Executando o metodo Consultar contido no objeto userData do qual retorna uma entidade.
                userData.PesquisarUsuario(userEntity);
                return userEntity;
            }
            catch
            {

                return null;
            }

        }
        public bool insertUsuario(UserEntity userEntity)
        {
            try
            {
                var userData = new UserData();
                userData.insertUsuario(userEntity);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        //public bool NovaSenha(UserEntity usuarioEntity)
        //{
        //    try
        //    {
        //        var userData = new UserData();
        //        userData.NovaSenha(usuarioEntity);
        //        return true;
        //    }
        //    catch (Exception)
        //    {
        //        return false;
        //    }
        //}

        //public bool LastLogin(UserEntity usuarioEntity)
        //{
        //    try
        //    {
        //        var userData = new UserData();
        //        userData.LastLogin(usuarioEntity);
        //        return true;
        //    }
        //    catch (Exception)
        //    {
        //        return false;
        //    }
        //}

        //public List<UserEntity> LastLoginTime(UserEntity usuarioEntity)
        //{
        //    try
        //    {
        //        var userData = new UserData();

        //        return userData.LastLoginTime(usuarioEntity);

        //    }
        //    catch (Exception)
        //    {
        //        return null;
        //    }
        //}

        //public List<UserEntity> NewsLogin(UserEntity usuarioEntity)
        //{
        //    try
        //    {
        //        var userData = new UserData();

        //        return userData.NewsLogin(usuarioEntity);

        //    }
        //    catch (Exception)
        //    {
        //        return null;
        //    }
        //}

        //public bool RegisterUser(UserEntity usuarioEntity)
        //{
        //    try
        //    {
        //        var userData = new UserData();
        //        userData.RegisterUser(usuarioEntity);
        //        return true;
        //    }
        //    catch (Exception)
        //    {
        //        return false;
        //    }
        //}
        //public UserEntity VerificadorDeEmail(UserEntity userEntity)
        //{
        //    try
        //    {
        //        // Criando uma instancia da classe User contida na camada data
        //        var userData = new UserData();

        //        // Executando o metodo Consultar contido no objeto userData do qual retorna uma entidade.
        //        userData.VerificadorDeEmail(userEntity);
        //        return userEntity;
        //    }
        //    catch
        //    {

        //        return null;
        //    }

        //}
    }
}
