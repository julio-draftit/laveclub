﻿using LavData;
using LavEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LavBfl
{
    public class CarrinhoBfl
    {
        public bool insertCarrinho(CarrinhoEntity carrinhoEntity)
        {
            try
            {
                var carrinhoData = new CarrinhoData();
                carrinhoData.insertCarrinho(carrinhoEntity);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public CarrinhoEntity countCarrinho(CarrinhoEntity carrinhoEntity)
        {
            try
            {
                // Criando uma instancia da classe User contida na camada data
                var carrinhoData = new CarrinhoData();

                // Executando o metodo Consultar contido no objeto userData do qual retorna uma entidade.
                carrinhoData.countCarrinho(carrinhoEntity);
                return carrinhoEntity;
            }
            catch
            {

                return null;
            }
        }
        public List<CarrinhoEntity> listCarrinho(CarrinhoEntity carrinhoEntity)
        {
            try
            {
                var carrinhoData = new CarrinhoData();

                return carrinhoData.listCarrinho(carrinhoEntity);

            }
            catch (Exception)
            {
                return null;
            }
        }
        public bool deleteCarrinho(CarrinhoEntity carrinhoEntity)
        {
            try
            {
                var carrinhoData = new CarrinhoData();
                carrinhoData.deleteCarrinho(carrinhoEntity);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
