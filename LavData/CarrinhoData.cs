﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LavEntity;
using LavData;

namespace LavData
{
    public class CarrinhoData : Contexto
    {
        public bool insertCarrinho(CarrinhoEntity carrinhoEntity)
        {
            Cnn().Open();
            var cmd = new SqlCommand();
            SqlDataReader dr = null;
            var sb = new StringBuilder();
            cmd.CommandTimeout = 1800;
            sb.Append("INSERT INTO [laveclub].[lavclub].[tbCarrinho] (UserID, ItemID, Quantidade, DateCreate, CarrinhoStatus) " +
                      "VALUES (" + carrinhoEntity.UserID + ", " + carrinhoEntity.ItemID + ", " + carrinhoEntity.Quantidade + ",GETDATE(), 'C')");
            try
            {
                cmd = new SqlCommand(sb.ToString(), Cnn());
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                Cnn().Close();
                dr.Close();
                sb.Clear();
            }
        }
        public CarrinhoEntity countCarrinho(CarrinhoEntity carrinhoEntity)
        {
            Cnn().Open();
            var cmd = new SqlCommand();
            SqlDataReader dr = null;
            var sb = new StringBuilder();
            cmd.CommandTimeout = 1800;

            sb.Append("SELECT Count(*) as Counts FROM [laveclub].[lavclub].[tbCarrinho] WHERE UserID = '" + carrinhoEntity.UserID + "' AND CarrinhoStatus = 'C'");
            try
            {
                cmd = new SqlCommand(sb.ToString(), Cnn());
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    carrinhoEntity.Counts = dr["Counts"].ToString();
                }
                else
                {
                    carrinhoEntity.Counts = "";
                }
                return carrinhoEntity;
            }
            catch
            {
                return null;
            }
            finally
            {
                Cnn().Close();
                dr.Close();
                sb.Clear();
            }

        }
        public List<CarrinhoEntity> listCarrinho(CarrinhoEntity carrinhoEntity)
        {
            var listarCarrinho = new List<CarrinhoEntity>();

            Cnn().Open();
            var cmd = new SqlCommand();
            var sb = new StringBuilder();
            cmd.CommandTimeout = 1800;
            SqlDataReader dr = null;

            sb.Append("SELECT c.*, i.ItemName, i.ItemPrice, i.ItemType, i.ItemImage "+
                    "FROM[laveclub].[lavclub].[tbCarrinho] c " +
                    "JOIN tbItens i " +
                    "ON c.ItemID = i.ItemID WHERE UserID = '" + carrinhoEntity.UserID + "' AND CarrinhoStatus = 'C'");
            try
            {
                cmd = new SqlCommand(sb.ToString(), Cnn());
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    var carEntity = new CarrinhoEntity();

                    carEntity.CarrinhoID = Convert.ToInt32(dr["CarrinhoID"]);
                    carEntity.ItemID = Convert.ToInt32(dr["ItemID"]);
                    carEntity.UserID = Convert.ToInt32(dr["UserID"]);
                    carEntity.Quantidade = Convert.ToInt32(dr["Quantidade"]);
                    carEntity.ItemName = dr["ItemName"].ToString();
                    carEntity.ItemPrice = dr["ItemPrice"].ToString();
                    carEntity.ItemType = dr["ItemType"].ToString();
                    carEntity.ItemImage = dr["ItemImage"].ToString();

                    listarCarrinho.Add(carEntity);
                }
                return listarCarrinho;
            }
            catch (Exception)
            {
                return null;
            }

        }
        public bool deleteCarrinho(CarrinhoEntity carrinhoEntity)
        {
            Cnn().Open();
            var cmd = new SqlCommand();
            SqlDataReader dr = null;
            var sb = new StringBuilder();
            cmd.CommandTimeout = 1800;
            sb.Append("DELETE FROM [laveclub].[lavclub].[tbCarrinho] WHERE CarrinhoID = " + carrinhoEntity.CarrinhoID);
            try
            {
                cmd = new SqlCommand(sb.ToString(), Cnn());
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                Cnn().Close();
                dr.Close();
                sb.Clear();
            }
        }
    }
}
