﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LavData
{
    public class Contexto
    {
        private SqlConnection _cnn;
        public SqlConnection Cnn()
        {
            return _cnn ?? (_cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["Lav"].ConnectionString));

        }
    }
}
