﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LavEntity;
using LavData;


namespace LavData
{
    public class UserData : Contexto
    {
        public UserEntity PesquisarUsuario(UserEntity userEntity)
        {
            Cnn().Open();
            var cmd = new SqlCommand();
            SqlDataReader dr = null;
            var sb = new StringBuilder();
            cmd.CommandTimeout = 1800;

            sb.Append("SELECT * FROM [laveclub].[lavclub].[tbUsers] WHERE UserMail = '"+ userEntity.UserMail + "' AND UserPassword = '" + userEntity.UserPassword + "' AND UserStatus = 'A'");
            try
            {
                cmd = new SqlCommand(sb.ToString(), Cnn());
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    userEntity.UserID = Convert.ToInt32(dr["UserID"]);
                    userEntity.UserMail = (dr["UserMail"].ToString());
                    userEntity.UserPerfil = (dr["UserPerfil"].ToString());
                    userEntity.UserPassword = (dr["UserPassword"].ToString());
                    userEntity.UserName = (dr["UserName"].ToString());
                }
                else
                {
                    userEntity.UserMail = "";
                    userEntity.UserPassword = "";
                    userEntity.UserName = "";
                }
                return userEntity;
            }
            catch
            {
                return null;
            }
            finally
            {
                Cnn().Close();
                dr.Close();
                sb.Clear();
            }

        }

        public bool insertUsuario(UserEntity usuarioEntity)
        {
            Cnn().Open();
            var cmd = new SqlCommand();
            SqlDataReader dr = null;
            var sb = new StringBuilder();
            cmd.CommandTimeout = 1800;
            sb.Append("INSERT INTO [laveclub].[lavclub].[tbUsers] (UserName,UserMail,UserPassword," +
                "UserStatus,UserDateCreate,UserPerfil,UserCEP,UserEndereco,UserNumero,UserBairro,UserCidade" +
                ",UserEstado,UserTelefone) VALUES (" +
                "'"+ usuarioEntity.UserName +"'," +
                "'" + usuarioEntity.UserMail + "'," +
                "'" + usuarioEntity.UserPassword + "'," +
                "'A'," +
                "GETDATE()," +
                "'3'," +
                "'" + usuarioEntity.UserCEP + "'," +
                "'" + usuarioEntity.UserEndereco + "'," +
                "'" + usuarioEntity.UserNumero + "'," +
                "'" + usuarioEntity.UserBairro + "'," +
                "'" + usuarioEntity.UserCidade + "'," +
                "'" + usuarioEntity.UserEstado + "'," +
                "'" + usuarioEntity.UserTelefone + "'" +
                ")");
            try
            {
                cmd = new SqlCommand(sb.ToString(), Cnn());
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                Cnn().Close();
                dr.Close();
                sb.Clear();
            }
        }

        //public bool LastLogin(UserEntity usuarioEntity)
        //{
        //    Cnn().Open();
        //    var cmd = new SqlCommand();
        //    SqlDataReader dr = null;
        //    var sb = new StringBuilder();
        //    cmd.CommandTimeout = 1800;
        //    sb.Append("UPDATE USER_REGISTER " +
        //              "SET UserLastLogin = '" + usuarioEntity.UserLastLogin +
        //              "' WHERE UserID = '" + usuarioEntity.UserID + "'");
        //    try
        //    {
        //        cmd = new SqlCommand(sb.ToString(), Cnn());
        //        cmd.CommandType = CommandType.Text;
        //        dr = cmd.ExecuteReader();

        //        return true;
        //    }
        //    catch (Exception)
        //    {
        //        return false;
        //    }
        //    finally
        //    {
        //        Cnn().Close();
        //        dr.Close();
        //        sb.Clear();
        //    }
        //}

        //public List<UserEntity> LastLoginTime(UserEntity usuarioEntity)
        //{
        //    var listaUserEntity = new List<UserEntity>();

        //    Cnn().Open();
        //    var cmd = new SqlCommand();
        //    var sb = new StringBuilder();
        //    cmd.CommandTimeout = 1800;
        //    SqlDataReader dr = null;

        //    sb.Append("SELECT TOP 20 * FROM USER_REGISTER " +
        //              " WHERE UserLastLogin IS NOT NULL ORDER BY UserLastLogin DESC");
        //    try
        //    {
        //        cmd = new SqlCommand(sb.ToString(), Cnn());
        //        cmd.CommandType = CommandType.Text;
        //        dr = cmd.ExecuteReader();
        //        while (dr.Read())
        //        {
        //            var userEntity = new UserEntity();


        //            userEntity.UserID = Convert.ToInt32(dr["UserID"]);
        //            userEntity.UserName = dr["UserName"].ToString();
        //            userEntity.UserLastLogin = dr["UserLastLogin"].ToString();


        //            listaUserEntity.Add(userEntity);
        //        }
        //        return listaUserEntity;
        //    }
        //    catch (Exception)
        //    {
        //        return null;
        //    }

        //}

        //public List<UserEntity> NewsLogin(UserEntity usuarioEntity)
        //{
        //    var listaUserEntity = new List<UserEntity>();

        //    Cnn().Open();
        //    var cmd = new SqlCommand();
        //    var sb = new StringBuilder();
        //    cmd.CommandTimeout = 1800;
        //    SqlDataReader dr = null;

        //    sb.Append("SELECT * FROM USER_REGISTER " +
        //              " where UserStatus = 'V'");
        //    try
        //    {
        //        cmd = new SqlCommand(sb.ToString(), Cnn());
        //        cmd.CommandType = CommandType.Text;
        //        dr = cmd.ExecuteReader();
        //        while (dr.Read())
        //        {
        //            var userEntity = new UserEntity();


        //            userEntity.UserID = Convert.ToInt32(dr["UserID"]);
        //            userEntity.UserName = dr["UserName"].ToString();
        //            userEntity.UserLastLogin = dr["UserLastLogin"].ToString();
        //            userEntity.UserBU = dr["UserBU"].ToString();


        //            listaUserEntity.Add(userEntity);
        //        }
        //        return listaUserEntity;
        //    }
        //    catch (Exception)
        //    {
        //        return null;
        //    }

        //}

        //public bool RegisterUser(UserEntity usuarioEntity)
        //{
        //    Cnn().Open();
        //    var cmd = new SqlCommand();
        //    SqlDataReader dr = null;
        //    var sb = new StringBuilder();
        //    cmd.CommandTimeout = 1800;
        //    sb.Append("INSERT INTO USER_REGISTER (" +
        //              " UserName, UserMail, UserPassword, UserPerfil, UserStatus, UserBU" +
        //              ")" +
        //              " VALUES" +
        //              " ('" + usuarioEntity.UserName + "','" + usuarioEntity.UserMail +
        //              "','" + usuarioEntity.UserPassword + "','" + usuarioEntity.UserPerfil +
        //              "','" + usuarioEntity.UserStatus + "','" + usuarioEntity.UserBU +
        //              "')");
        //    try
        //    {
        //        cmd = new SqlCommand(sb.ToString(), Cnn());
        //        cmd.CommandType = CommandType.Text;
        //        dr = cmd.ExecuteReader();

        //        return true;
        //    }
        //    catch (Exception)
        //    {
        //        return false;
        //    }
        //    finally
        //    {
        //        Cnn().Close();
        //        dr.Close();
        //        sb.Clear();
        //    }
        //}

        //public UserEntity VerificadorDeEmail(UserEntity userEntity)
        //{
        //    Cnn().Open();
        //    var cmd = new SqlCommand();
        //    SqlDataReader dr = null;
        //    var sb = new StringBuilder();
        //    cmd.CommandTimeout = 1800;

        //    sb.Append("SELECT * FROM USER_REGISTER WHERE LTRIM(UserMail) = " + "'" + userEntity.UserMailValidar.Trim() + "'");
        //    try
        //    {
        //        cmd = new SqlCommand(sb.ToString(), Cnn());
        //        cmd.CommandType = CommandType.Text;
        //        dr = cmd.ExecuteReader();
        //        if (dr.Read())
        //        {
        //            userEntity.UserID = Convert.ToInt32(dr["UserID"]);
        //            userEntity.UserMail = (dr["UserMail"].ToString());
        //            userEntity.UserPassword = (dr["UserPassword"].ToString());
        //        }
        //        else
        //        {
        //            userEntity.UserID = 0;
        //            userEntity.UserMail = "";
        //            userEntity.UserPassword = "";
        //        }
        //        return userEntity;
        //    }
        //    catch
        //    {
        //        return null;
        //    }
        //    finally
        //    {
        //        Cnn().Close();
        //        dr.Close();
        //        sb.Clear();
        //    }

        //}
    }
}
