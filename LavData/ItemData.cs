﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LavEntity;
using LavData;

namespace LavData
{
    public class ItemData : Contexto
    {
        public List<ItemEntity> listItens(ItemEntity itemEntity)
        {
            var listarItem = new List<ItemEntity>();

            Cnn().Open();
            var cmd = new SqlCommand();
            var sb = new StringBuilder();
            cmd.CommandTimeout = 1800;
            SqlDataReader dr = null;

            sb.Append("SELECT * FROM [laveclub].[lavclub].[tbItens]");
            try
            {
                cmd = new SqlCommand(sb.ToString(), Cnn());
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    var itensEntity = new ItemEntity();


                    itensEntity.ItemID = Convert.ToInt32(dr["ItemID"]);
                    itensEntity.ItemName = dr["ItemName"].ToString();
                    itensEntity.ItemPrice = dr["ItemPrice"].ToString();
                    itensEntity.ItemType = dr["ItemType"].ToString();
                    itensEntity.ItemImage = dr["ItemImage"].ToString();

                    listarItem.Add(itensEntity);
                }
                return listarItem;
            }
            catch (Exception)
            {
                return null;
            }

        }
    }
}
